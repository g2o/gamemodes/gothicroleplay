
local focusDraw = Draw(3134,6904,"LEFT CONTROL aby porozmawia");
local focusNpc = -1;

local focusTexture = Texture(3000,6824,focusDraw.width+268,300, "MENU_INGAME.TGA");
focusTexture.setColor(55,5,5);

function showNpcDialog()
{
    focusTexture.visible = true;
    focusDraw.visible = true;
    focusDraw.top();
}

function hideNpcDialog()
{
    focusTexture.visible = false;
    focusDraw.visible = false;
}

addEventHandler("onKey", function(key) {
    if(key != KEY_LCONTROL)
        return;

    if(focusNpc == -1)
        return;

    if(Player.gui != -1)
        return;

    if(getPlayerWeaponMode(heroId) != 0)
        return;

    if(focusTexture.visible)
        DialogManager.openForNPC(getPlayerName(focusNpc));
})

addEventHandler("onFocus", function(new, old) {
    if(new == -1 && focusNpc != -1)
    {
        focusNpc = -1;
        hideNpcDialog();
        return;
    }

    if(new < getMaxSlots())
        return;
    
    focusNpc = new;

    if(getPlayerName(new) in DialogManager.Npcs)
        showNpcDialog();
})

function getFocusedNpc()
{
    return focusNpc;
}
