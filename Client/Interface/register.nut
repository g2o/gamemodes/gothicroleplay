
Interface.Register <- {
    window = GUI.Window(anx(Resolution.x/2-250), any(Resolution.y/2 - 300), anx(500), any(460), "MENU_INGAME.TGA", null, false)

    "show" : function() {
        window.setVisible(true);
    }

    "hide" : function() {
        window.setVisible(false);
    }
};

Interface.Register.logo <- GUI.Draw(anx(70), any(-40), CFG.Hostname, Interface.Register.window)
Interface.Register.usernameDraw <- GUI.Draw(anx(20), any(10), _L("Username"), Interface.Register.window)
Interface.Register.passwordDraw <- GUI.Draw(anx(20), any(110), _L("Password"), Interface.Register.window)
Interface.Register.passwordRepeatDraw <- GUI.Draw(anx(20), any(210), _L("Password"), Interface.Register.window)

Interface.Register.username <- GUI.Input(anx(20), any(30), anx(460), any(50), "DLG_CONVERSATION.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "...", 6, Interface.Register.window)
Interface.Register.password <- GUI.Input(anx(20), any(130), anx(460), any(50), "DLG_CONVERSATION.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Password, Align.Left, "...", 6, Interface.Register.window)
Interface.Register.passwordRepeat <- GUI.Input(anx(20), any(230), anx(460), any(50), "DLG_CONVERSATION.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Password, Align.Left, "...", 6, Interface.Register.window)

Interface.Register.exitButton <- GUI.Button(anx(20), any(320), anx(210), any(50), "DLG_CONVERSATION.TGA", _L("Exit"), Interface.Register.window)
Interface.Register.registerButton <- GUI.Button(anx(270), any(320), anx(210), any(50), "DLG_CONVERSATION.TGA", _L("Register"), Interface.Register.window)
Interface.Register.loggInButton <- GUI.Button(anx(20), any(390), anx(460), any(50), "DLG_CONVERSATION.TGA", _L("Logg In"), Interface.Register.window)

Interface.Register.logo.setFont("FONT_OLD_20_WHITE_HI.TGA");
Interface.Register.logo.setColor(200, 0, 0);
Interface.Register.window.setColor(255, 0, 0);

addEventHandler("GUI.onClick", function(self)
{
    if(Player.gui != PLAYER_GUI.REGISTER)
        return;

	switch (self)
	{
		case Interface.Register.exitButton:
			exitGame();
		break;

		case Interface.Register.loggInButton:
			Interface.Register.hide();
			Interface.LoggIn.show();
            Player.gui = PLAYER_GUI.LOGGIN;
		break;

		case Interface.Register.registerButton:
			if(Interface.Register.password.getText() != Interface.Register.passwordRepeat.getText())
			{
				intimate(_L("Incorrect password."));
				return;
			}
			if(Interface.Register.username.getText().len() < 3 || Interface.Register.password.getText().len() < 3)
			{
				intimate(_L("Incorrect password or username."));
				return;
			}
			Player.packetRegister(Interface.Register.username.getText(), md5(Interface.Register.password.getText()));
		break;
	}
})

addEventHandler("onChangeLanguage", function(lang) {
	Interface.Register.usernameDraw.setText(_L("Username"))
	Interface.Register.passwordDraw.setText(_L("Password"))
	Interface.Register.passwordRepeat.setText(_L("Password"))

	Interface.Register.exitButton.setText(_L("Exit"))
	Interface.Register.loggInButton.setText(_L("Logg In"))
	Interface.Register.registerButton.setText(_L("Register"))
});