
Database <- {};

function Database::getNextId()
{
    try
    {
        local pfile = file("database/id.cfg", "r");

        local id = pfile.read("l").tointeger(); id = id + 1;
        pfile.close();

        pfile = file("database/id.cfg", "w");
        pfile.write(id+"\n");
        pfile.close();

        return id;
    }
    catch (errorMsg)
    {
        local pfile = file("database/id.cfg", "w");
        pfile.write("1");
        pfile.close();

        return 1;
    }
}

function Database::checkExistPlayer(name)
{
    try
    {
        local pfile = file("database/"+ name + ".txt", "r");

        local id = pfile.read("l");
        return pfile.read("l");
    }
    catch (errorMsg) 
        return false;
}

function Database::createPlayer(player)
{
    local pos = getPlayerPosition(player.pid);
    local visual = getPlayerVisual(player.pid);
	local pfile = file("database/"+ getPlayerName(player.pid) + ".txt", "w+");
    pfile.write(player.id + "\n");
    pfile.write(player.password + "\n");
    pfile.write(player.classId + "\n");
    pfile.write(player.fractionId + "\n");
    pfile.write(player.walk + "\n");
    pfile.write(pos.x + "\n");
    pfile.write(pos.y + "\n");
    pfile.write(pos.z + "\n");
    pfile.write(getPlayerAngle(player.pid) + " \n")
    pfile.write(player.str + "\n");
    pfile.write(player.dex + "\n");
    pfile.write(getPlayerHealth(player.pid) + "\n");
    pfile.write(player.hpMax + "\n");
    pfile.write(getPlayerMana(player.pid) + "\n");
    pfile.write(player.manaMax + "\n");
    pfile.write(visual.bodyModel + "\n");
    pfile.write(visual.bodyTxt + "\n");
    pfile.write(visual.headModel + "\n");
    pfile.write(visual.headTxt + "\n");
    pfile.write(player.weapon[0] + "\n");
    pfile.write(player.weapon[1] + "\n");
    pfile.write(player.weapon[2] + "\n");
    pfile.write(player.weapon[3] + "\n");
    pfile.write(player.magicLvl + "\n");
    pfile.write(player.description + "\n");
    pfile.close();
}

function Database::updatePlayer(player)
{
    try
	{
        local pos = getPlayerPosition(player.pid);
        local visual = getPlayerVisual(player.pid);
        local pfile = file("database/"+ getPlayerName(player.pid) + ".txt", "w");
		pfile.write(player.id + "\n");
		pfile.write(player.password + "\n");
		pfile.write(player.classId + "\n");
		pfile.write(player.fractionId + "\n");
		pfile.write(player.walk + "\n");
        pfile.write(pos.x + "\n");
        pfile.write(pos.y + "\n");
        pfile.write(pos.z + "\n");
        pfile.write(getPlayerAngle(player.pid) + " \n")
		pfile.write(player.str + "\n");
		pfile.write(player.dex + "\n");
		pfile.write(getPlayerHealth(player.pid) + "\n");
		pfile.write(player.hpMax + "\n");
		pfile.write(getPlayerMana(player.pid) + "\n");
		pfile.write(player.manaMax + "\n");
        pfile.write(visual.bodyModel + "\n");
		pfile.write(visual.bodyTxt + "\n");
		pfile.write(visual.headModel + "\n");
		pfile.write(visual.headTxt + "\n");
		pfile.write(player.weapon[0] + "\n");
		pfile.write(player.weapon[1] + "\n");
		pfile.write(player.weapon[2] + "\n");
		pfile.write(player.weapon[3] + "\n");
		pfile.write(player.magicLvl + "\n");
		pfile.write(player.description + "\n");
		pfile.close();
        return true;
	}
	catch (errorMsg)
		return false;
}

function Database::removeAccount(player)
{
    try
	{
        local pfile = file("database/"+ getPlayerName(player.pid) + ".txt", "w");
        pfile.close();
        return true;
    }
    catch (errorMsg)
        return false;
}


function Database::loadAccount(player)
{
    local pid = player.pid;
	local pfile = file("database/"+ getPlayerName(pid) + ".txt", "r");
	
    player.id = pfile.read("l").tointeger();
    player.password = pfile.read("l");
    player.classId = pfile.read("l").tointeger();
    player.fractionId = pfile.read("l").tointeger();
    setPlayerWalkingStyle(pid, pfile.read("l").tointeger());
    
    local posX = pfile.read("l").tointeger();
    local posY = pfile.read("l").tointeger();
    local posZ = pfile.read("l").tointeger();
    local angle = pfile.read("l").tointeger();
    setPlayerPosition(pid, posX, posY, posZ);
    setPlayerAngle(pid, angle);

    setPlayerStrength(pid, pfile.read("l").tointeger());
    setPlayerDexterity(pid, pfile.read("l").tointeger());
    setPlayerHealth(pid, pfile.read("l").tointeger());
    setPlayerMaxHealth(pid, pfile.read("l").tointeger());
    setPlayerMana(pid, pfile.read("l").tointeger());
    setPlayerMaxMana(pid, pfile.read("l").tointeger());
    
    local body = pfile.read("l");
    local skin = pfile.read("l").tointeger();
    local head = pfile.read("l");
    local face = pfile.read("l").tointeger();

    setPlayerVisual(pid, body, skin, head, face);
    setPlayerSkillWeapon(pid, 0, pfile.read("l").tointeger())
    setPlayerSkillWeapon(pid, 1, pfile.read("l").tointeger())
    setPlayerSkillWeapon(pid, 2, pfile.read("l").tointeger())
    setPlayerSkillWeapon(pid, 3, pfile.read("l").tointeger())
    setPlayerMagicLevel(pid, pfile.read("l").tointeger())
    setPlayerDescription(pid, pfile.read("l"));

    local packet = Packet()
    packet.writeUInt8(PacketId.Player);
    packet.writeUInt8(PacketPlayer.SetClass);
    packet.writeInt16(player.fractionId);
    packet.writeInt16(player.classId);
    packet.send(pid, RELIABLE_ORDERED);
    packet = null;       
}

function Database::saveItems(pid)
{
    try
	{
        local fileSave = file("database/items/"+getPlayerName(pid)+".txt", "w");

		foreach(v,k in getPlayerItems(pid))
			fileSave.write(Items.name(v) + " " + k + "\n");
		
	    fileSave.close();
	}
    catch (errorMsg)
        print(errorMsg)	
}

function Database::loadItems(pid)
{
    try
	{
        local fileLoad = file("database/items/"+getPlayerName(pid)+".txt", "r");
	    local args = 0;

	    do{
            args = fileLoad.read("l");		
		    if(args != null){
				local arg = sscanf("sd", args);
				giveItem(pid, arg[0], arg[1]);
		    }		    
            else{
                break;
            }
        } while (args != null)   

        fileLoad.close();
	}
    catch (errorMsg) {}
}