local registeredItemsGround = [];

class registerGroundItem
{
    left = null;
    itemLeft = null;
    instance = null;

    item = null;

    constructor(x,y,z)
    {
        left = -1;
        itemLeft = 180;

        local finalItem = getRandomItem();
        
        if(finalItem == false)
            left = 1;
        else {
            item = ItemsGround.spawn(finalItem.id, finalItem.amount, x, y, z, "NEWWORLD\\NEWWORLD.ZEN");
            itemLeft = finalItem.left;
        }

        registeredItemsGround.append(this);
    }

    function getAllCFGGroundCount()
    {
        local amount = 0;
        foreach(item in CFG.GroundItems)
            amount += item[2];

        return amount;
    }

    function getRandomItem()
    {
        local randomNumber = rand() % getAllCFGGroundCount();
        local amount = 0;

        foreach(item in CFG.GroundItems)
        {
            if(randomNumber >= amount && randomNumber < (amount + item[2]))
                return {id = Items.id(item[0]), amount = item[1], left = item[3]}; 

            amount += item[2];
        }
        return false;
    }

    function onTake()
    {
        left = itemLeft;
    }

    function respawn()
    {
        local finalItem = getRandomItem();
        left = -1;

        if(finalItem == false)
            left = 1;
        else {
            item = ItemsGround.spawn(finalItem.id, finalItem.amount, x, y, z, "NEWWORLD//NEWWORLD.ZEN");
            itemLeft = finalItem.left;
        }       
    }
}

addEventHandler("onPlayerTakeItem", function(pid, item) {
    foreach(itemGround in registeredItemsGround)
    {
        if(itemGround.item != item)
            continue;

        itemGround.onTake();
    }
})

addEventHandler("onMinute", function () {
    foreach(itemGround in registeredItemsGround)
    {
        if(itemGround.left == -1)
            continue;
        
        itemGround.left = itemGround.left - 1;
        if(itemGround.left <= 1)
            itemGround.respawn();
    }
})

function getRegisteredHerbs()
{
    return registeredItemsGround;
}